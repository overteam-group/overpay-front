import { createContext } from "react";

const HeadContext = createContext();

export default HeadContext;
