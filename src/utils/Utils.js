import axios from "axios";

export const getUuid = (nick) => {
  return axios.get(`https://api.mojang.com/users/profiles/minecraft/${nick}`);
};

const Utils = getUuid;

export default Utils;
