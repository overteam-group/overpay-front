import axios from "axios";

const url = "https://tokyo.overmc.net";

export const buyItem = (payload) => {
  return axios.post(`${url}/payment/newTransaction`, payload);
};

export const getCategories = () => {
  return axios.get(`${url}/category/get`, {
    headers: {
      "Content-Type": "application/json",
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

export const getItems = () => {
  return axios.get(`${url}/item/get`, {
    headers: {
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

export const getUsers = () => {
  return axios.get(`${url}/user/get`, {
    headers: {
      "Content-Type": "application/json",
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

export const getUser = (nick) => {
  return axios.get(`${url}/user/getByNickname?nickname=${nick}`, {
    headers: {
      "Content-Type": "application/json",
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

export const getItem = (id) => {
  return axios.get(
    `${url}/item/getById`,
    {
      headers: {
        "Content-Type": "application/json",
        authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
      },
    },
    {
      params: {
        Id: id,
      },
    }
  );
};

export const getCategoryItems = (id) => {
  return axios.get(`${url}/item/getByCategoryId`, {
    headers: {
      categoryId: id,
    },
  });
};

export const paymentAfter = () => {
  return axios.get(`${url}/payment/after`);
};

export const getPayMethods = () => {
  return axios.get("https://sandbox.przelewy24.pl/api/v1/payment/methods/pl", {
    headers: {
      "Content-Type": "application/json",
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

export const buyNow = (nick, id, email) => {
  const payload = {
    nickname: nick,
    itemId: id,
    urlReturn: "https://mc.x-nation.org/oczekiwanie",
    email: email,
    description: "Doladowanie srodkow",
    header: {
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  };

  return buyItem(payload);
};

export const getHead = (nick) => {
  return axios.get(`${url}/utils/fetchPlayerHead?name=${nick}`, {
    headers: {
      "Content-Type": "arraybuffer",
      authorization: "Basic YXBpOjdNOXRMNipGKlZYRCEyYSY=",
    },
  });
};

const PayUtils =
  (buyItem,
  getCategories,
  getItems,
  getUsers,
  getCategoryItems,
  paymentAfter,
  buyNow,
  getHead);

export default PayUtils;
