import React from 'react'
import "./PriceList.scss"
import Footer from '../footer/Footer'

const PriceList = () => {

    return (
        <>
      <div className="price-list">

        <h1>Cennik</h1>

        <ul>
            <li>5 PLN - 5 Coins</li>
            <li>10 PLN - 10 Coins</li>
            <li>20 PLN - 20 Coins</li>
            <li>50 PLN - 50 Coins</li>
            <li>100 PLN - 100 Coins</li>
        </ul>

      </div>
      <Footer />
      </>
    )
    
}

export default PriceList;