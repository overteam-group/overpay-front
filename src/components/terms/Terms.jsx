import React from 'react'
import Menu from '../menu/Menu'
import './Terms.scss'

const Terms = () => {

    return (
        <>
            <div className="header-bg"></div>
            <Menu />
            <div className='terms'>
                <div className='terms-bg'>
                    <h1>Regulamin płatności</h1>

                    <h2>Zasady &amp; Warunki Płatności</h2>

                    <p>
                    * Monety - Wirtualna waluta obowiązująca na serwerch gier x-nation.org
                    </p>
                    <ul>
                        <li>
                            <b>1.</b> Przed wykonaniem transakcji masz obowiązek przeczytać niniejszy regulamin. 
                        </li>
                        <li>
                            <b>2.</b> W przypadku nie otrzymania coinsów lub innej usługi pomimo zapłaty w ciągu kilku minut, należy zgłosić się do Administracji x-nation.org lub Discord oraz napisać reklamacje na stronie usługodawcy przelewy24.pl. 
                        </li>
                        <li>
                            <b>3.1.</b> Za udany przelew otrzymujesz dostęp do Monet na serwerach mc.x-nation.org
                        </li>
                        <li>
                            <b>3.2.</b> Poprawnie zakończona transakcja daje dostęp do Monet zgodnie z cennikiem. Monety nadal są własnością serwera mc.x-nation.org
                        </li>
                        <li>
                            <b>4.</b> W przypadku zamknięcia serwera, zmiany wersji, zmiany mapy, zmiany rodzaju rozgrywek na mc.x-nation.org, Monety mogo zostać utracone i nie podlegają zwrotowi.
                        </li>
                        <li>
                            <b>5.</b> Gracz może stracić Monety, jeżeli zostanie zablokowany poprzez np. blokadę konta.
                        </li>
                        <li>
                            <b>6.</b> Gracz otrzymuje dostęp do Monet na podany nick. Administracja nie przenosi graczowi Monet na inny nick.
                        </li>
                        <li>
                            <b>7.</b> Jeżeli gracz zrobi tz. FRAUD w premium SMS (oszustwo związane ze zwrotem środków u operatora) lub wyłudzi zwrot środków pośrednicząc z jakimś organem np. bankiem, jego nick zostaje wpisany na czarną listę serwera x-nation.org (brak możliwości unbana).
                        </li>
                        <li>
                            <b>8.</b> Każdy numer, który zakupił przedmiot za pomocą SMS zostaje dodany do listy. Administracja ma prawo wysyłać treści reklamowe do numerów z listy. (Istnieje możliwość wypisania się z listy poprzez kontakt na Discord)
                        </li>
                        <li>
                            <b>10.1.</b> Zastrzegamy sobie prawo do zmiany nazwy, ceny i uprawnień rang, bez żadnego powiadomienia.
                        </li>
                        <li>
                            <b>10.2.</b> Zapewniamy, że zmiany te są i mogą mieć miejsce w celu zrównoważenia serwera. 
                        </li>
                        <li>
                            <b>11.</b> Wspierający serwer nie są zwolnieni z regulaminu, wszelkie odstępstwa od niego prowadzą do kar, a nawet blokady konta na serwerze.
                        </li>
                        <li>
                            <b>12.</b> Administracja nie ponosi odpowiedzialności za bezpieczeństwo konta gracza / Monety / przedmioty.
                        </li>
                        <li>
                            <b>13.</b> Administacja serwera nie zezwala na udostępnianie konta użytkownika osobom trzecim. 
                        </li>
                        <li>
                            <b>14.</b> Przedmioty oraz rangi, na wyznaczonych serwerach typu: MegaDrop itd. nie są trwałe, po zresetowaniu postępu lub nowej edycji nie są zwracane. 
                        </li>
                        <li>
                            <b>15.</b> Administarcja serwera zastrzega sobie prawo do zmiany warunków regulaminu bez żadnego powiadomienia. 
                        </li>
                        <li>
                            <b>16.</b> Wysyłając SMS Premium, dokonując platności PayPal, Przelew Tradycyjny lub PaySafeCard oświadczasz, że nie będziesz rościł od Administracji żadnych odszkodowań.
                        </li>
                        <li>
                            <b>17.</b> Wirtualna waluta Coins, którą można nabyć w sklepie(mc.x-nation.org) w grze, umożliwia kupowanie zawartości od twórców podczas gry na serwerach Minecraft - x-Nation.org. Coins są przechowywane w Twoim wirtualnym portfelu i mogą być używane do kupowania przedmiotów w grze Minecraft.
                        </li>
                        <li>
                            <b>18.</b> Każdy dokonany zakup jest powiązany z Twoim kontem w grze na serwerach Minecraft - x-Nation.org.
                        </li>
                    </ul>
                    
                    <h2>Tryb postępowania reklamacyjnego</h2>

                    <ul>
                        Reklamacje związane ze świadczeniem Usług Elektronicznych oraz Aplikacji przez Usługodawcę:
                        <li>
                            <b>▪</b> Reklamacje związane ze świadczeniem Usług Elektronicznych oraz Aplikacji za pośrednictwem Serwisu Użytkownik może składać za pośrednictwem poczty elektronicznej na adres: kontakt@x-nation.org
                        <li>
                        </li>
                            <b>▪</b>  W powyższej wiadomości e-mail, należy podać jak najwięcej informacji i okoliczności dotyczących przedmiotu reklamacji, w szczególności rodzaj i datę wystąpienia nieprawidłowości oraz dane kontaktowe. Podane informacje znacznie ułatwią i przyspieszą rozpatrzenie reklamacji przez Usługodawcę.
                        </li>
                        <li>
                            <b>▪</b>  Rozpatrzenie reklamacji przez Usługodawcę następuje niezwłocznie, nie później niż w terminie 14 dni.
                        </li>
                        <li>
                            <b>▪</b>  Odpowiedź Usługodawcy w sprawie reklamacji jest wysyłana na adres e-mail Użytkownika podany w zgłoszeniu reklamacyjnym lub w inny podany przez Użytkownika sposób.
                        </li>
                    </ul>

                    <ul className='contact'>
                        <li>
                            Właścicielem serwisu jest: Zybel sp. z o.o.
                        </li>
                        <li>
                            NIP: 5291802504
                        </li>
                        <li>
                            E-mail: kontakt@x-nation.org
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default Terms;