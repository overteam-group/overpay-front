import React, { useState, useEffect, useContext } from "react";
import "./Head.scss";
import MoneyContext from "../../contexts/MoneyContext";
import { getUser, getHead } from "../../utils/PayUtils";
import Loading from "../loading/Loading";

const Head = ({ nick }) => {
  const [src, setImg] = useState([]);
  const [money, setMoney] = useContext(MoneyContext);

  useEffect(() => {
    getUser(nick).then((res) => setMoney(res.data.money / 100));
    getHead(nick).then((res) => {
      setImg(Buffer.from(res.data, "binary").toString("base64"));
    });
  }, []);

  return (
    <div className="head">
      <img
        src={`https://tokyo.overmc.net/utils/fetchPlayerHead?name=${nick}`}
        alt=""
        width="108px"
        height="98px"
      />
      <div className="player-info">
        <p>{nick}</p>
        <span>{money} zł</span>
      </div>
    </div>
  );
};

export default Head;
