import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router";
import NickContext from "../../contexts/NickContext";
import OffersContext from "../../contexts/OffersContext";
import { getItems } from "../../utils/PayUtils";
import Menu from "../menu/Menu";
import "./Login.scss";

const Login = () => {
  const [nick, setNick] = useContext(NickContext);
  // eslint-disable-next-line
  const [offers, setOffers] = useContext(OffersContext);

  let history = useHistory();

  useEffect(() => {
    getItems().then((res) => setOffers(res.data));
    // eslint-disable-next-line
  }, []);

  const userLogin = () => {
    if (nick === "") return;
    if (nick.length < 3 || !/^[a-zA-Z0-9_]+$/.test(nick)) {
      alert("Podaj poprawny nick");
      return;
    }
    history.push("/sklep");
  };

  return (
    <>
      <div className="header-bg"></div>
      <Menu />
      <div className="container">
        <div className="container-top"></div>
        <div className="demo-flex-spacer"></div>
        <div className="container-bg">
          <h1>Zaloguj się</h1>
          <p>Wprowadź swój nick z serwera</p>
          <form
            name="login-form"
            onSubmit={(e) => {
              e.preventDefault();
              userLogin();
            }}
          >
            <div className="webflow-style-input">
              <input
                className=""
                type="text"
                placeholder="Twój nick w grze"
                onChange={(e) => {
                  setNick(e.target.value);
                }}
              ></input>
              <button type="submit">
                <i>→</i>
              </button>
            </div>
          </form>
        </div>
        <div className="demo-flex-spacer"></div>
        <div className="demo-flex-spacer"></div>
        <a
          className="demo"
          href=""
          title="X-Nation.org"
          onClick={(e) => e.preventDefault()}
        >
          <b>mc.x-nation.org</b> nie jest w żaden sposób powiązany z Mojang, AB.
        </a>
      </div>
    </>
  );
};

export default Login;
