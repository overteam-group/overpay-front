import React from "react";
import { Spinner } from "react-bootstrap";
import "./Waiting.scss";

const Waiting = () => {
  return (
    <div className="waiting">
      <h1>Oczekiwanie na potwierdzenie</h1>
      <Spinner animation="border" variant="primary" />
      <p>Możesz zamknąć ten formularz...</p>
    </div>
  );
};

export default Waiting;
