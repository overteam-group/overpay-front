import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <>
      <div className="footer">
        <p>
          <b>mc.x-nation.org</b> nie jest w żaden sposób powiązany z Mojang, AB.
        </p>
      </div>
    </>
  );
};

export default Footer;
