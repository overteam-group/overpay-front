import React, { useContext } from "react";
import Footer from "../footer/Footer";
import NickContext from "../../contexts/NickContext";
import OffersContext from "../../contexts/OffersContext";
import MoneyContext from "../../contexts/MoneyContext";
import Head from "../../components/head/Head";
import "./Offers.scss";
import Offer from "../offer/Offer";
import Loading from "../loading/Loading";
import Menu from "../menu/Menu";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

const Offers = () => {
  const [nick, setNick] = useContext(NickContext);
  // eslint-disable-next-line
  const [offers, setOffers] = useContext(OffersContext);
  // eslint-disable-next-line
  const [money, setMoney] = useContext(MoneyContext);

  const logout = () => {
    setNick("");
  };

  if (!offers["0"]) return <Loading />;

  return (
    <>
      <div className="header-bg"></div>
      <Menu />
      <div className="offers-container">
        <div className="top-content">
          <div className="profile">
            <Head nick={nick} />
            <div
              className="logout"
              onClick={() => {
                logout();
              }}
            >
              <span></span>
              <i>
                <FontAwesomeIcon icon={faSignOutAlt} />
              </i>
            </div>
          </div>
          <div className="offer-wrapper">
            {offers.map((offer, key) => {
              return <Offer key={key} offer={offer} />;
            })}
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Offers;
