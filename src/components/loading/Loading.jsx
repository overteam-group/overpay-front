import React from "react";
import { Spinner } from "react-bootstrap";
import "./Loading.scss";

const Loading = () => {
  return (
    <div className="loading">
      <Spinner animation="border" variant="primary" />
      <p>Loading...</p>
    </div>
  );
};

export default Loading;
