import React, { useContext } from "react";
import { useHistory } from "react-router";
import NickContext from "../../contexts/NickContext";
import "./Menu.scss";

const Menu = ({ bar }) => {
  const [nick, setNick] = useContext(NickContext);

  let history = useHistory();

  const redirect = (route) => {
    history.push({ pathname: `/${route}` });
  };

  const shop = () => {
    if (nick !== "") redirect("sklep");
    else redirect("logowanie");
  };

  if (window.innerWidth < 800) return <div></div>;

  return (
    <div className="menu">
      <div className="menu-left"></div>
      <div className="menu-right">
        <ul>
          <li className="choose">Strona główna</li>
          <li
            className="choose"
            onClick={() => {
              shop();
            }}
          >
            Sklep
          </li>
          <li className="disabled">Panel gracza</li>
          <li
            className="choose"
            onClick={() => {
              redirect("regulamin");
            }}
          >
            Regulamin
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Menu;
