import React, { useContext, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./BuyForm.scss";
import NickContext from "../../contexts/NickContext";
import { buyNow } from "../../utils/PayUtils";
import { useLocation, useHistory } from "react-router";
import Menu from "../menu/Menu";
import Footer from "../footer/Footer";

const BuyForm = () => {
  const [email, setEmail] = useState("");
  // eslint-disable-next-line
  const [nick, setNick] = useContext(NickContext);
  
  let history = useHistory();

  const redirect = (url) => {
    window.location = url;
  };

  const search = useLocation().search;

  return (
    <>
    <div className="header-bg"></div>
      <Menu />
      <div className="buy">
        <div className="buy-left"></div>
          <div className="buy-form">
            <div className="buy-bg">
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  if (nick === "") {
                    history.push("/logowanie")
                  }
                  else if (email === "") {
                    alert("Musisz podać email")
                  }
                  else if (document.getElementById('confirm-terms').checked === true) {
                  const item = new URLSearchParams(search).get("item");
                  buyNow(nick, item, email).then((res) => {
                    redirect(res.data.link);
                  });
                }
                else {
                  alert("Musisz zaakceptować Regulamin płatności")
                }
                }}
              >
              <div className="form-group">
                <label htmlFor="exampleInputEmail1">Twój nick</label>
                <input
                  type="text"
                  className="form-control readonly"
                  id="nickname"
                  aria-describedby="emailHelp"
                  readOnly
                  placeholder={nick}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleInputPassword1">Twój email</label>
                <input
                  type="email"
                  className="form-control"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  id="exampleInputPassword1"
                  placeholder="Email"
                />
              </div>
              <div className="form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="confirm-terms"
                />
                <label className="form-check-label" htmlFor="exampleCheck1">
                  Akceptuję Regulamin*
                </label>
                <br />
                <small id="emailHelp" className="form-text text-muted" onClick={() => {history.push("/regulamin")}}>
                  Regulamin płatności
                </small>
              </div>
              <div className="redirect">
                <button type="submit" className="btn btn-primary">
                  Przejdź do płatności
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="buy-right"></div>
      </div>
    </>
  );
};

export default BuyForm;
