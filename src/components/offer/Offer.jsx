import React from "react";
import { Card, Button } from "react-bootstrap";
import { useHistory } from "react-router";
import Money1 from "./images/Money1.png";
import Money2 from "./images/Money2.png";
import Money3 from "./images/Money3.png";
import Money4 from "./images/Money4.png";
import Money5 from "./images/Money5.png";
import "./Offer.scss";

const Offer = (offer) => {
  let history = useHistory();

  const buy = () => {
    history.push({ pathname: "/zakup", search: `?item=${offer.offer.id}` });
  };

  let images = [];

  const style = { width: "17.5rem", padding: "10px 20px" };

  if (window.innerWidth < 1500) style.width = "15rem";

  images.push(Money1);
  images.push(Money2);
  images.push(Money3);
  images.push(Money4);
  images.push(Money5);

  return (
    <div id={offer.offer.id} className={`offer s${offer.offer.id}`}>
      <Card className="bg-dark text-white text-center spec-offer" style={style}>
        <Card.Img variant="top" src={images[offer.offer.id - 1]} />
        <Card.Body>
          <Card.Title
            style={{
              marginTop: "-8px",
              paddingBottom: "30px",
              fontSize: "18px",
            }}
          >
            {offer.offer.name}
          </Card.Title>
          {/* <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text> */}
          <Button
            onClick={() => buy()}
            variant="primary"
            style={{
              fontSize: "14px",
              padding: "8px 16px",
              marginBottom: "7px",
            }}
          >
            Doładuj
          </Button>
        </Card.Body>
      </Card>
    </div>
  );
};

export default Offer;
