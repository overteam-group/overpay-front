import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import React, { useState } from "react";
import Login from "./components/login/Login";
import Offers from "./components/offers/Offers";
import NickContext from "./contexts/NickContext";
import OffersContext from "./contexts/OffersContext";
import HeadContext from "./contexts/HeadContext";
import BuyForm from "./components/buy-form/BuyForm";
import Waiting from "./components/waiting/Waiting";
import Terms from "./components/terms/Terms";
import PriceList from "./components/price-list/PriceList";
import MoneyContext from "./contexts/MoneyContext";

function App() {
  const [nick, setNick] = useState("");
  const [offers, setOffers] = useState([]);
  const [head, setHead] = useState();
  const [money, setMoney] = useState(null);

  if (window.location.pathname === "/oczekiwanie") return <Waiting />;

  return (
    <Router>
      <Switch>
        <NickContext.Provider value={[nick, setNick]}>
          {nick === "" && <Redirect to="/logowanie" />}
          <Route path="/oczekiwanie" component={Waiting} />
          <OffersContext.Provider value={[offers, setOffers]}>
            <MoneyContext.Provider value={[money, setMoney]}>
              <HeadContext.Provider value={[head, setHead]}>
                <Route path="/logowanie" component={Login} />
                <Route path="/cennik" component={PriceList} />
                <Route path="/sklep" component={Offers} />
                <Route path="/zakup" component={BuyForm} />
                <Route path="/regulamin" component={Terms} />
              </HeadContext.Provider>
            </MoneyContext.Provider>
          </OffersContext.Provider>
        </NickContext.Provider>
      </Switch>
    </Router>
  );
}

export default App;
